const express = require('express');
const fs = require('fs');
let posts = require('./db/posts.json');
const app = express();
const port = 3000;

//middleware
app.use(express.json());

app.get('/api/v1/posts', (req, res) => {
    res.status(200).json(posts);
});

app.get('/api/v1/posts/:id', (req, res) => {
    let detailPost = posts.find(el => el.id === parseInt(req.params.id))
    res.status(200).json(detailPost);
});

app.post('/api/v1/posts', (req, res) => {
    fs.readFile("./db/posts.json", 'utf8', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            obj = JSON.parse(data);
            dataInput = req.body;
            let id = {id: posts.length+1};
            dataInput = {...id, ...dataInput}
            obj.push(dataInput);
            json = JSON.stringify(obj);
            fs.writeFile('./db/posts.json', json, (err) => {
                if (err) throw err;
            });

            res.status(200).json({
                message: "post has been successfuly posted",
                data: req.body
            });
        }
    });
});


app.put('/api/v1/posts/:id', (req, res) => {

    let newPosts = posts.map(data => {
        if (data.id === parseInt(req.params.id)) {
            data.nama = req.body.nama
            data.hoby = req.body.hoby
        }
        return data;
    });

    fs.writeFile('./db/posts.json', JSON.stringify(newPosts), (err) => {
        if (err) throw err;
        res.status(200).json({
            message: "post has been successfuly edited",
            data: req.body
        });
    });

});

app.delete('/api/v1/posts/:id', (req, res) => {

    let newPosts = posts.filter(data => data.id != parseInt(req.params.id));

    fs.writeFile('./db/posts.json', JSON.stringify(newPosts), (err) => {
        if (err) throw err;
        res.status(200).json({ message: "post has been successfuly deleted" });
    });

});


app.listen(port, () => {
    console.log('Server nyala di port 3000');
})
